﻿using System;
using System.Reflection;
using System.Runtime.InteropServices;

namespace Managed
{
	class Program
	{
		private delegate void Callback();

		[DllImport("__Internal")]
		private static extern void set_callback(Callback callback);

		static void Main(string[] args)
		{
			Console.WriteLine(Assembly.GetExecutingAssembly());
			set_callback(CallbackFunc);
		}

		static void CallbackFunc()
		{
			Console.WriteLine(Assembly.GetExecutingAssembly());
		}
	}
}
